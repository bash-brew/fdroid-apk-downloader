.PHONY:		all
.SILENT:

default:
			clear
			bash Scripts/print_help.sh			# Print makefile usage

list:
			clear
			bash Scripts/fdroid_list.sh			# List apps in Apks/ and fdroid.plist

add:		list
			bash Scripts/fdroid_add.sh			# Add app to fdroid.plist and Apks/
			make update

remove:		list
			bash Scripts/fdroid_remove.sh		# Remove app from fdroid.plist and Apks/

update:
			clear
			bash Scripts/fdroid_download.sh		# Download fdroid.plist to Apks/

install:	list
			bash Scripts/adb_sideload.sh		# Sideload Apks/ to ADB device

commit:
			clear
			bash Scripts/readme_generate.sh		# Generate README.md
			git add --all						# Commit repo changes
			git commit -m "Apps updated."
			git push ; echo
