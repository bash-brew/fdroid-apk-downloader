#!/bin/bash

main() {
	adb_sideload
}

adb_sideload() {
	ls Apks/*.apk | xargs -I {} adb install "{}"
}

main
